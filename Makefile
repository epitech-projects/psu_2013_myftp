##
## Makefile for myftp in /home/gravie_j/projets/PSU_2013_myftp
##
## Made by Jean Gravier
## Login   gravie_j<gravie_j@epitech.net>
##
## Started on  Mon Apr  7 11:48:47 2014 Jean Gravier
## Last update Thu Apr 10 17:41:04 2014 Jean Gravier
##

CLIENT	=	client/
SERVEUR	=	serveur/

all:
	make -C $(CLIENT)
	make -C $(SERVEUR)

clean:
	make -C $(CLIENT) clean
	make -C $(SERVEUR) clean

fclean:
	make -C $(CLIENT) fclean
	make -C $(SERVEUR) fclean

re:
	make -C $(CLIENT) re
	make -C $(SERVEUR) re
