/*
** client.h for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 15:43:50 2014 Jean Gravier
** Last update Sun Apr 13 19:33:10 2014 Jean Gravier
*/

#ifndef CLIENT_H_
# define CLIENT_H_

# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <unistd.h>

enum	command_type
  {
    NORMAL,
    PUT,
    GET,
    QUIT
  };

int	create_connection(char *, char *);
int	put_error(char *);
int	prompt(int, char *, char *);
int	get(int);
int	put(char *, int);

#endif /* !CLIENT_H_ */
