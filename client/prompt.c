/*
** prompt.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 21:48:54 2014 Jean Gravier
** Last update Sun Apr 13 23:14:07 2014 Jean Gravier
*/

#include "client.h"
#include "common.h"

int		parse_cmd(char *cmd)
{
  if (!strncmp(cmd, "quit\n", 5))
    return (QUIT);
  else if (!strncmp(cmd, "put ", 4))
    return (PUT);
  else if (!strncmp(cmd, "get ", 4))
    return (GET);
  return (NORMAL);
}

int		prompt(int fd, char *host, char *port)
{
  int		cmd_type;
  char		*line;
  FILE		*server;
  ssize_t	size;
  char		cmd_prompt[BUFFER_SIZE];

  size = 0;
  if ((server = fdopen(fd, "w+")) == NULL)
    return (put_error("fdopen"));
  snprintf(cmd_prompt, 4096,
	   MAGENTA "%s" RESET ":" GREEN "%s" RESET "--> ", host, port);
  write(1, cmd_prompt, strlen(cmd_prompt));
  while ((size = getline(&line, (size_t *)&size, stdin)) != -1)
    {
      if ((cmd_type = parse_cmd(line)) == QUIT)
	break;
      dprintf(fd, "%s", line);
      size = 0;
      cmd_type == GET ? get(fd) : cmd_type == PUT ? put(&line[3], fd) : 1;
      if ((size = getdelim(&line, (size_t *)&size, 0, server)) != -1)
	write(1, line, size);
      size = 0;
      write(1, cmd_prompt, strlen(cmd_prompt));
    }
  return (EXIT_SUCCESS);
}
