/*
** error.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 17:00:14 2014 Jean Gravier
** Last update Tue Apr  8 17:24:11 2014 Jean Gravier
*/

#include "client.h"

int		put_error(char *str)
{
  perror(str);
  return (EXIT_FAILURE);
}
