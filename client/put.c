/*
** put.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Sat Apr 12 19:22:28 2014 Jean Gravier
** Last update Sun Apr 13 22:22:04 2014 Jean Gravier
*/

#include <sys/stat.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include "client.h"
#include "common.h"

size_t		getfilesize(t_file *file)
{
  struct stat	buf;

  if (stat(file->name, &buf) == -1)
    {
      file->failed = 1;
      return (put_error("stat"));
    }
  return (buf.st_size);
}

int		put(char *filename, int server_fd)
{
  int		file_fd;
  t_file	file;
  int		pong;

  if (filename[0] == ' ')
    filename++;
  if (filename[strlen(filename) - 1] == '\n')
    filename[strlen(filename) - 1] = 0;
  memset(&file, 0, sizeof(file));
  strncpy(file.name, filename, BUFFER_SIZE);
  file.failed = 0;
  file.size = getfilesize(&file);
  if (file.failed)
    return (EXIT_FAILURE);
  printf ("file.failed: %d\n", file.failed);
  if (write(server_fd, &file, sizeof(file)) == -1)
      return (put_error("put"));
  if ((file_fd = open(file.name, O_RDONLY)) == -1)
    return (put_error("put"));
  if (sendfile(server_fd, file_fd, NULL, file.size) == -1)
    return (put_error("put"));
  write(server_fd, &pong, sizeof(int));
  if (close(file_fd) == -1)
    return (put_error("put"));
  return (EXIT_SUCCESS);
}
