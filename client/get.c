/*
** get.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Fri Apr 11 16:15:30 2014 Jean Gravier
** Last update Sun Apr 13 22:45:47 2014 Jean Gravier
*/

#include <sys/stat.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include "client.h"
#include "common.h"

int		get(int fd)
{
  char		buffer[BUFFER_SIZE];
  size_t	wrote;
  int		ret;
  int		file_fd;
  t_file	file;

  wrote = 0;
  usleep(10);
  if (read(fd, &file, sizeof(file)) != sizeof(file))
    return (put_error("read"));
  if (file.failed)
    return (EXIT_FAILURE);
  if ((file_fd = open(file.name, O_RDWR | O_CREAT | O_TRUNC, 0644)) == -1)
    return (put_error("open"));
  while (wrote < file.size && (ret = read(fd, buffer, BUFFER_SIZE)) != -1)
    {
      if ((wrote += ret) <= file.size)
	write(file_fd, buffer, ret);
      else
	write(file_fd, buffer, file.size - (wrote - ret));
    }
  write(fd, &ret, sizeof(int));
  if (close(file_fd) == -1 || ret == -1)
    return (put_error("get"));
  return (EXIT_SUCCESS);
}
