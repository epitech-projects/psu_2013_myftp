/*
** main.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Mon Apr  7 16:50:36 2014 Jean Gravier
** Last update Tue Apr  8 20:32:27 2014 Jean Gravier
*/

#include "client.h"

int	main(int argc, char *argv[])
{
  if (argc > 2)
    create_connection(argv[1], argv[2]);
  else
    printf("Usage : ./client machine port\n");
  return (0);
}
