/*
** network.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 15:56:37 2014 Jean Gravier
** Last update Sun Apr 13 23:15:52 2014 Jean Gravier
*/

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "client.h"

int			connect_to(int fd, char *host, char *port)
{
  struct sockaddr_in	s_in;

  s_in.sin_family = AF_INET;
  s_in.sin_port = htons(atoi(port));
  if (inet_aton(host, &s_in.sin_addr) == 0)
    {
      printf("invalid address format: %s", host);
      return (EXIT_FAILURE);
    }
  if (connect(fd, (struct sockaddr *)&s_in, sizeof(s_in)) == -1)
    return (put_error("connect"));
  prompt(fd, host, port);
  return (EXIT_SUCCESS);
}

int			create_connection(char *host, char *port)
{
  struct protoent	*pe;
  int			fd;

  if ((pe = getprotobyname("TCP")) == NULL)
    return (EXIT_FAILURE);
  if ((fd = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (put_error("socket"));
  if (connect_to(fd, host, port) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  if (close(fd) == -1)
    return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}
