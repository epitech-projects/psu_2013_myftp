/*
** get.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Fri Apr 11 14:45:03 2014 Jean Gravier
** Last update Sun Apr 13 22:52:52 2014 Jean Gravier
*/

#include <sys/stat.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include "serveur.h"
#include "commands.h"

size_t		getfilesize(t_file *file)
{
  struct stat	buf;

  if (stat(file->name, &buf) == -1)
    {
      file->failed = 1;
      return (put_error("stat"));
    }
  return (buf.st_size);
}

int		get(char *filename, int fd)
{
  int		file_fd;
  t_file	file;
  int		pong;

  if (filename[0] == ' ')
    filename++;
  if (filename[strlen(filename) - 1] == '\n')
    filename[strlen(filename) - 1] = 0;
  strncpy(file.name, filename, BUFFER_SIZE);
  file.failed = 0;
  file.size = getfilesize(&file);
  write(fd, &file, sizeof(file));
  if (file.failed)
    return (EXIT_FAILURE);
  if ((file_fd = open(file.name, O_RDONLY)) == -1)
    return (put_error("open"));
  if (sendfile(fd, file_fd, NULL, file.size) == -1)
    return (put_error("sendfile"));
  read(fd, &pong, sizeof(int));
  if (close(file_fd) == -1)
    return (put_error("close"));
  return (EXIT_SUCCESS);
}
