/*
** change_dir.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 14:08:46 2014 Jean Gravier
** Last update Sun Apr 13 14:20:24 2014 Jean Gravier
*/

#include <limits.h>
#include <stdlib.h>
#include "commands.h"
#include "serveur.h"

int	check_path(char *path, char *homepath)
{
  char	*real;

  if ((real = realpath(path, NULL)) == NULL)
    return (put_error("path"));
  if (strncmp(real, homepath, strlen(homepath)))
    return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}

int	change_dir(char *path, char *homepath)
{
  if (path[0] == ' ')
    path++;
  if (path[strlen(path) - 1] == '\n')
    path[strlen(path) - 1] = 0;
  if (check_path(path, homepath) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  if (chdir(path) == -1)
    return (put_error(path));
  return (EXIT_SUCCESS);
}
