/*
** current_dir.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 14:34:49 2014 Jean Gravier
** Last update Sat Apr 12 21:40:22 2014 Jean Gravier
*/

#include <limits.h>
#include "commands.h"
#include "serveur.h"

int	current_dir(int fd, int newline)
{
  char	buffer[PATH_MAX];

  if (getcwd(buffer, PATH_MAX) == NULL)
    return (put_error("pwd"));
  dprintf(fd, "%s", buffer);
  if (newline)
    dprintf(fd, "%c", '\n');
  return (EXIT_SUCCESS);
}
