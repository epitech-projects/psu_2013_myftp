/*
** commands.h for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 13:51:54 2014 Jean Gravier
** Last update Sun Apr 13 14:20:32 2014 Jean Gravier
*/

#ifndef COMMANDS_H_
# define COMMANDS_H_

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <string.h>
# include <errno.h>

int	list(int);
int	change_dir(char *, char *);
int	current_dir(int, int);
int	put(int);
int	get(char *, int);

#endif /* !COMMANDS_H_ */
