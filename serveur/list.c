/*
** list.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 13:51:26 2014 Jean Gravier
** Last update Sun Apr 13 18:13:58 2014 Jean Gravier
*/

#include <dirent.h>
#include "commands.h"
#include "serveur.h"

int		list(int fd)
{
  int		i;
  int		n;
  struct dirent **namelist;

  i = 0;
  n = scandir("./", &namelist, NULL, alphasort);
  if (n < 0)
    return (put_error("scandir"));
  else
    {
      while (i < n)
	{
	  dprintf(fd, "%s\n", namelist[i]->d_name);
	  free(namelist[i++]);
	}
      free(namelist);
    }
  return (EXIT_SUCCESS);
}
