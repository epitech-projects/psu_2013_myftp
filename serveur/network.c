/*
** network.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 15:56:37 2014 Jean Gravier
** Last update Sun Apr 13 16:35:48 2014 Jean Gravier
*/

#include "serveur.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

int			wait_client(int fd)
{
  pid_t			pid;
  int			client_fd;
  char			*client_ip;
  socklen_t		in_size;
  struct sockaddr_in	client_in;

  pid = 1;
  while (pid != 0)
    {
      if ((client_fd = accept(fd, (struct sockaddr *)&client_in, &in_size)) == -1)
	return (put_error("accept"));
      pid = fork();
      if (!pid)
	{
	  client_ip = inet_ntoa(client_in.sin_addr);
	  printf("Client %s connected !\n", client_ip);
	  prompt_client(client_fd);
	}
    }
  return (EXIT_SUCCESS);
}

int			bind_name(int fd, char *port)
{
  struct sockaddr_in	in;

  in.sin_family = AF_INET;
  in.sin_port = htons(atoi(port));
  in.sin_addr.s_addr = INADDR_ANY;
  if (bind(fd, (struct sockaddr *)&in, sizeof(in)) == -1)
    return (put_error("bind"));
  return (EXIT_SUCCESS);
}

int			create_connection(char *port)
{
  struct protoent	*pe;
  int			fd;

  if ((pe = getprotobyname("TCP")) == NULL)
    return (EXIT_FAILURE);
  if ((fd = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (put_error("socket"));
  if (bind_name(fd, port) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  if (listen(fd, 1) == -1)
    return (put_error("listen"));
  if (wait_client(fd) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  if (close(fd) == -1)
    return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}
