/*
** prompt.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 13:25:57 2014 Jean Gravier
** Last update Sun Apr 13 23:18:11 2014 Jean Gravier
*/

#include <limits.h>
#include <unistd.h>
#include "serveur.h"
#include "commands.h"

int		parse(char *cmd, int fd, char *homepath)
{
  if (!strcmp(cmd, "ls\n"))
    return (list(fd));
  else if (!strncmp(cmd, "cd ", 3))
    return (change_dir(&cmd[2], homepath));
  else if (!strcmp(cmd, "pwd\n"))
    return (current_dir(fd, 1));
  else if (!strncmp(cmd, "get ", 4))
    return (get(&cmd[3], fd));
  else if (!strncmp(cmd, "put ", 4))
    return (put(fd));
  return (-1);
}

int		prompt_client(int client_fd)
{
  int		ret;
  char		*cmd;
  ssize_t	size;
  FILE		*client;
  char		homepath[PATH_MAX];

  cmd = NULL;
  if (getcwd(homepath, PATH_MAX) == NULL)
    return (put_error("getcwd"));
  if ((client = fdopen(client_fd, "w+")) == NULL)
    return (put_error("fderror"));
  size = 0;
  while ((size = getline(&cmd, (size_t *)&size, client)) != -1)
    {
      ret = parse(cmd, client_fd, homepath);
      ret == EXIT_SUCCESS ? dprintf(client_fd, GREEN "SUCCESS\n%c" RESET, 0) :
	ret == EXIT_FAILURE ? dprintf(client_fd, RED "FAILED\n%c" RESET, 0) :
	dprintf(client_fd, "command not found: %s%c", cmd, 0);
      size = 0;
      if (cmd != NULL)
	free(cmd);
    }
  if (cmd != NULL)
    free(cmd);
  return (EXIT_SUCCESS);
}
