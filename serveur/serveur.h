/*
** serveur.h for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 13:29:48 2014 Jean Gravier
** Last update Sun Apr 13 14:20:46 2014 Jean Gravier
*/

#ifndef SERVEUR_H_
# define SERVEUR_H_

# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include "common.h"

int	create_connection(char *);
int	prompt_client(int);
int	put_error(char *);

#endif /* !SERVEUR_H_ */
